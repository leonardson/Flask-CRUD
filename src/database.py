import MySQLdb
import MySQLdb.cursors

HOST = 'db4free.net'
USER = 'leonardson'
PASS = 'leonardson'
DB = 'testespython'

def connect():
    # Creating connection
    conn = MySQLdb.connect(host=HOST, user=USER, passwd=PASS, cursorclass=MySQLdb.cursors.DictCursor)
    cursor = conn.cursor()

    cursor.execute('USE {}'.format(DB))
    cursor.execute("""SELECT table_name AS 'table',  column_name AS  'fk', 
            referenced_table_name AS 'reftable', referenced_column_name  AS 'refpk' 
            FROM information_schema.key_column_usage
            WHERE referenced_table_name IS NOT NULL 
            AND TABLE_SCHEMA='{}'""".format(DB))
    tables = cursor.fetchall()    

    print(tables)
    return conn
from flask import Flask
app = Flask(__name__)

# My modules for routes
from install import Install

@app.route('/')
def route_hello_world():
    return 'Hello, World!'

@app.route('/install')
def route_install():
    return Install()